#include "movement.h"

Movement::Movement(QWidget *parent) : QWidget(parent)
{
    QFont *font = new QFont("DejaVu Sans");
    font->setBold(true);
    font->setPixelSize(20);
    last_move = new QLabel(this);
    last_move->setFont(*font);
    last_move->setText("Ostatnio wykryty ruch:\n  47 min temu");

    plot = new QCustomPlot(this);
    plot->plotLayout()->insertRow(0);
    plot->plotLayout()->addElement(0, 0, new QCPTextElement(plot, "Wykryty ruch", QFont("sans", 10, QFont::Normal)));
    plot->xAxis->setLabel("Czas");
    plot->yAxis->setLabel("Ruch");
    plot->yAxis->setRange(0, 1);
    plot->xAxis->setRange(0, 100);
    plot->replot();

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(last_move);
    vbox->addWidget(plot);
    this->setLayout(vbox);
}

void Movement::addMove(bool move)
{

}
