#ifndef MOVEMENT_H
#define MOVEMENT_H

#include <QWidget>
#include <QVBoxLayout>
#include <QGraphicsView>
#include <QFont>
#include <QLabel>
#include "qcustomplot.h"

//using namespace QtCharts;

class Movement : public QWidget
{
    Q_OBJECT
public:
    explicit Movement(QWidget *parent = nullptr);
    float value;
signals:

public slots:
    void addMove(bool move);
private:
    QLabel *last_move;
    QCustomPlot *plot;
};

#endif // MOVEMENT_H
