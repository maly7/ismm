#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QHBoxLayout>
#include <QPixmap>
#include <QPainter>
#include <QString>
#include <QLabel>
#include <QFont>
#include <QDebug>

const QString kolory[] = {"0055ff","114fee","2249dd","3344cc","443ebb","5538aa","663399","772d88",
                          "882777","992266","aa1c55","bb1644","cc1133","dd0b22","ee0511","ff0000"};

/**
 * \brief Klasa dziedzicząca po QWidget odpowiadająca za wyświetlanie temperaturę.
 * \author Marcin Węgrzyn
 */
class Temperature : public QWidget
{
    Q_OBJECT
public:
    explicit Temperature(QWidget *parent = nullptr);
signals:

public slots:
    void setValue(float val);
private:
    /**
     * \brief QLabel odpowiadający wyświetlaniu tekstu o temperaturze.
     * \author Marcin Węgrzyn
     */
    QLabel *temp_txt;
    /**
     * \brief QLabel prostokąt, którego tło wypełniane jest odpowiednim kolorem, zmienia wysokość wraz ze zmianą wartości temperatury.
     * \author Marcin Węgrzyn
     */
    QLabel *termo_rect;
    /**
     * \brief QLabel w którym jako tło ustawione jest termometr o odpowiednim kolorze.
     * \author Marcin Węgrzyn
     */
    QLabel *termo_bg;
    /**
     * \brief Wartość float odpowiadająca aktualnej temperaturze.
     * \author Marcin Węgrzyn
     */
    float temp;
};

#endif // TEMPERATURE_H
