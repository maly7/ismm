#ifndef ISMM_SET_H
#define ISMM_SET_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QMessageBox>

namespace Ui {
class ISMM_SET;
}

/**
 * \brief Struktura Stacji zapisująca jej id, nazwe oraz adres.
 *
 * \author Karol Oleśkiewicz
 *
 * Struktura ta przydatna jest przy wyświetlaniu poszczególnej stacji, oraz jej łatwej edycji.
 */
struct Station {
    int id;
    QString name;
    int address;
};

/**
 * \brief Klasa odpowiadająca za wyświetlanie i edycję ustawień.
 * \author Karol Oleśkiewicz
 */
class ISMM_SET : public QDialog
{
    Q_OBJECT

public:
    explicit ISMM_SET(QWidget *parent = nullptr);
    ~ISMM_SET();

signals:
    void exitValue();

private slots:
    void on_pushButton_4_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_lineEdit_textChanged(const QString &arg1);
    void on_lineEdit_2_textChanged(const QString &arg1);
    void on_comboBox_currentIndexChanged(int index);
    void refreshCombo(void);

private:
    Ui::ISMM_SET *ui;
    QSqlDatabase bdb;
    QSqlQuery *query;
    /**
     * \brief Ilość stacji która ma zostać zapisana.
     * \author Marcin Węgrzyn
     */
    int new_count;
    /**
     * \brief Wektor w którym zapisywane są stacje.
     * \author Marcin Węgrzyn
     */
    QVector <Station> stations;
};

#endif // ISMM_SET_H
