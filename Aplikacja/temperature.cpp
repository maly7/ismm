#include "temperature.h"

Temperature::Temperature(QWidget *parent) : QWidget(parent)
{
    temp_txt = new QLabel(this);
    termo_bg = new QLabel(this);
    termo_rect = new QLabel(termo_bg);
    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addWidget(temp_txt);
    hbox->addWidget(termo_bg);
    hbox->setAlignment(termo_bg, Qt::AlignCenter);
    hbox->setAlignment(temp_txt, Qt::AlignCenter);

    this->setLayout(hbox);


    termo_bg->setStyleSheet("border-image: url(:/images/term0.png);;");
    termo_rect->setStyleSheet("border-image: none; background-color: #D42727; border-radius: 15;");
    termo_bg->setMinimumSize(100, 200);

    temp = 0;

    QFont *font = new QFont("DejaVu Sans");
    font->setBold(true);
    font->setPixelSize(48);
    temp_txt->setFont(*font);
    temp_txt->setText(tr("%1\u00B0C").arg(temp));

    temp = 0.0;
    setValue(temp);
}

/**
 * \brief Metoda odpowiadająca za ustawienie odpowiedniej temperatury.
 *
 * \author Marcin Węgrzyn
 *
 * Metoda ustawia odpowiednio wypełnienie termometru, zmienia jego kolor oraz edytuje tekst informujący o temperaturze.
 *
 * \param[in] val Wartość informująca o temperaturze z zakresu 15-30 st. C.
 */
void Temperature::setValue(float val)
{
    temp = val;
    QString text;
    text.sprintf("%.1f\u00B0C", temp);
    temp_txt->setText(text);

    if(val < 15) val = 15;
    else if(val > 30) val = 30;

    /* PRZY TERMO_BG 100x200
       val = MAX 30 MIN  15
       h   = MAX 18 MIN 118 */
    int h = 100-((val-15)*(100)/15)+18;
    termo_rect->setGeometry(34, h, 32, 150-h);

    int i = (int)(val-15);
    termo_bg->setStyleSheet("border-image: url(:/images/term"+QString::number(i)+".png);");
    termo_rect->setStyleSheet("border-image: none; background-color: #"+kolory[i]+"; border-radius: 15;");
}
