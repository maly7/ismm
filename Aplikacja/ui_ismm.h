/********************************************************************************
** Form generated from reading UI file 'ismm.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ISMM_H
#define UI_ISMM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ISMM
{
public:
    QAction *actionHistoria;
    QAction *actionUstawienia;
    QAction *actionWyj_cie;
    QAction *actionInformacje;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *lbl_Title;
    QTabWidget *tabWidget;
    QWidget *tab;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout;
    QLabel *lbl_Date;
    QLabel *lbl_Time;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton_4;
    QSpacerItem *verticalSpacer_2;
    QPushButton *pushButton_3;
    QSpacerItem *verticalSpacer_3;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QMenu *menuISMM;
    QMenu *menuO_Programie;

    void setupUi(QMainWindow *ISMM)
    {
        if (ISMM->objectName().isEmpty())
            ISMM->setObjectName(QStringLiteral("ISMM"));
        ISMM->resize(1030, 650);
        ISMM->setStyleSheet(QLatin1String("QTabBar::tab {\n"
"	height: 150px;\n"
"	width:80px;\n"
"}"));
        actionHistoria = new QAction(ISMM);
        actionHistoria->setObjectName(QStringLiteral("actionHistoria"));
        actionUstawienia = new QAction(ISMM);
        actionUstawienia->setObjectName(QStringLiteral("actionUstawienia"));
        actionWyj_cie = new QAction(ISMM);
        actionWyj_cie->setObjectName(QStringLiteral("actionWyj_cie"));
        actionInformacje = new QAction(ISMM);
        actionInformacje->setObjectName(QStringLiteral("actionInformacje"));
        centralWidget = new QWidget(ISMM);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        lbl_Title = new QLabel(centralWidget);
        lbl_Title->setObjectName(QStringLiteral("lbl_Title"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lbl_Title->sizePolicy().hasHeightForWidth());
        lbl_Title->setSizePolicy(sizePolicy);
        lbl_Title->setMinimumSize(QSize(500, 0));
        QFont font;
        font.setPointSize(16);
        lbl_Title->setFont(font);

        horizontalLayout->addWidget(lbl_Title);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 2);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setBaseSize(QSize(0, 0));
        tabWidget->setAutoFillBackground(false);
        tabWidget->setTabPosition(QTabWidget::West);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setIconSize(QSize(16, 16));
        tabWidget->setElideMode(Qt::ElideNone);
        tabWidget->setTabBarAutoHide(false);
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        tabWidget->addTab(tab_2, QString());

        gridLayout->addWidget(tabWidget, 1, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        lbl_Date = new QLabel(centralWidget);
        lbl_Date->setObjectName(QStringLiteral("lbl_Date"));
        lbl_Date->setFont(font);
        lbl_Date->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbl_Date);

        lbl_Time = new QLabel(centralWidget);
        lbl_Time->setObjectName(QStringLiteral("lbl_Time"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lbl_Time->sizePolicy().hasHeightForWidth());
        lbl_Time->setSizePolicy(sizePolicy1);
        lbl_Time->setMinimumSize(QSize(140, 0));
        QFont font1;
        font1.setPointSize(35);
        lbl_Time->setFont(font1);
        lbl_Time->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lbl_Time);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setMinimumSize(QSize(140, 70));

        verticalLayout->addWidget(pushButton_4);

        verticalSpacer_2 = new QSpacerItem(20, 25, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(140, 70));

        verticalLayout->addWidget(pushButton_3);

        verticalSpacer_3 = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_3);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMinimumSize(QSize(140, 70));
        pushButton->setStyleSheet(QStringLiteral("background-color: #FF7777;"));

        verticalLayout->addWidget(pushButton);


        gridLayout->addLayout(verticalLayout, 1, 1, 1, 1);

        ISMM->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ISMM);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1030, 22));
        menuISMM = new QMenu(menuBar);
        menuISMM->setObjectName(QStringLiteral("menuISMM"));
        menuO_Programie = new QMenu(menuBar);
        menuO_Programie->setObjectName(QStringLiteral("menuO_Programie"));
        ISMM->setMenuBar(menuBar);

        menuBar->addAction(menuISMM->menuAction());
        menuBar->addAction(menuO_Programie->menuAction());
        menuISMM->addAction(actionHistoria);
        menuISMM->addAction(actionUstawienia);
        menuISMM->addSeparator();
        menuISMM->addAction(actionWyj_cie);
        menuO_Programie->addAction(actionInformacje);

        retranslateUi(ISMM);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ISMM);
    } // setupUi

    void retranslateUi(QMainWindow *ISMM)
    {
        ISMM->setWindowTitle(QApplication::translate("ISMM", "ISMM", 0));
        actionHistoria->setText(QApplication::translate("ISMM", "Historia", 0));
        actionUstawienia->setText(QApplication::translate("ISMM", "Ustawienia", 0));
        actionWyj_cie->setText(QApplication::translate("ISMM", "Wyj\305\233cie", 0));
        actionInformacje->setText(QApplication::translate("ISMM", "Informacje", 0));
        lbl_Title->setText(QApplication::translate("ISMM", "Inteligentny System Monitorowania Mieszkania", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("ISMM", "Pomieszczenie 1", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("ISMM", "Pomieszczenie 2", 0));
        lbl_Date->setText(QApplication::translate("ISMM", "10.10.2017r.", 0));
        lbl_Time->setText(QApplication::translate("ISMM", "18:30", 0));
        pushButton_4->setText(QApplication::translate("ISMM", "Historia", 0));
        pushButton_3->setText(QApplication::translate("ISMM", "Ustawienia", 0));
        pushButton->setText(QApplication::translate("ISMM", "Wyjd\305\272", 0));
        menuISMM->setTitle(QApplication::translate("ISMM", "ISMM", 0));
        menuO_Programie->setTitle(QApplication::translate("ISMM", "O Programie", 0));
    } // retranslateUi

};

namespace Ui {
    class ISMM: public Ui_ISMM {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ISMM_H
