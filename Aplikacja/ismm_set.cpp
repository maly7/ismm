#include "ismm_set.h"
#include "ui_ismm_set.h"

#include <QDebug>

ISMM_SET::ISMM_SET(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ISMM_SET)
{
    ui->setupUi(this);

    bdb = QSqlDatabase::database();
    bdb.setDatabaseName("database.db");
    query = new QSqlQuery();
    if(!bdb.isOpen()) bdb.open();
    query->exec("SELECT * FROM Stations");
    while (query->next())
    {
        Station tmp;
        tmp.id = query->value(0).toInt();
        tmp.name = query->value(1).toString();
        tmp.address = query->value(2).toInt();
        stations.push_back(tmp);
    }

    new_count = 0;
    refreshCombo();
}

ISMM_SET::~ISMM_SET()
{
    bdb.close();
    delete query;
    delete ui;
}

/**
 * \brief Metoda wywoływana po wciśnięciu przycisku Zapisz.
 * \author Karol Oleśkiewicz
 *
 * Metoda aktualizuje dane dla każdej stacji pomiarowej oraz zapisuje je w bazie danych.
 *
 */
void ISMM_SET::on_pushButton_3_clicked()
{
    if(!bdb.isOpen()) bdb.open();
    for(int i = 0; i < stations.size(); i++)
        query->exec("REPLACE INTO Stations (ID, Name, Address) VALUES ("+QString::number(stations[i].id)+",'"+stations[i].name+"',"+QString::number(stations[i].address)+");");
    emit(exitValue());
    close();
}

void ISMM_SET::on_pushButton_4_clicked()
{
    close();
}

/**
 * \brief Metoda wywoływana przy tworzeniu nowej stacji pomiarowej.
 * \author Karol Oleśkiewicz
 *
 * Metoda dodaje nową stację pogodową.
 *
 */
void ISMM_SET::on_pushButton_clicked()
{
    Station tmp;
    tmp.name = "Nowa stacja";
    tmp.address = 0;

    if(!bdb.isOpen()) bdb.open();
    query->exec("SELECT MAX(ID) FROM Stations");
    query->next();
    new_count++;
    tmp.id = query->value(0).toInt()+new_count;

    stations.push_back(tmp);
    ui->comboBox->addItem(tmp.name);
    ui->comboBox->setCurrentIndex(ui->comboBox->count()-1);
    ui->lineEdit->setText(tmp.name);
    ui->lineEdit_2->setText(QString::number(tmp.address));
}

/**
 * \brief Metoda wywoływana przy usuwaniu stacji pomiarowej.
 * \author Karol Oleśkiewicz
 *
 * Metoda usuwa aktualnie wybraną stację pogodową.
 *
 */
void ISMM_SET::on_pushButton_2_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Usuwanie Stacji!", "Czy napewno chcesz usunąć stację "+stations[ui->comboBox->currentIndex()].name+" ?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
    {
        if(!bdb.isOpen()) bdb.open();
        query->exec("DELETE FROM Stations WHERE ID = "+QString::number(stations[ui->comboBox->currentIndex()].id)+";");
        stations.erase(stations.begin()+ui->comboBox->currentIndex());
        refreshCombo();
    }
}

void ISMM_SET::on_lineEdit_textChanged(const QString &arg1)
{
    stations[ui->comboBox->currentIndex()].name = arg1;
    ui->comboBox->setItemText(ui->comboBox->currentIndex(), arg1);
}

void ISMM_SET::on_lineEdit_2_textChanged(const QString &arg1)
{
    stations[ui->comboBox->currentIndex()].address = arg1.toInt();
}

/**
 * \brief Metoda odświeżająca dane stacji pogodowej.
 * \author Karol Oleśkiewicz
 *
 * Po zmianie wyświetlanej stacji pogodowej, metoda wpisuje dane stacji pogodowej do odpowiednich elementów (lineEdit).
 *
 */
void ISMM_SET::on_comboBox_currentIndexChanged(int index)
{
    if(index >= 0 && index < stations.size())
    {
        ui->lineEdit->setText(stations[index].name);
        ui->lineEdit_2->setText(QString::number(stations[index].address));
    }
}

/**
 * \brief Metoda odświeżająca listę stacji pogodowych.
 * \author Karol Oleśkiewicz
 */
void ISMM_SET::refreshCombo(void)
{
    ui->comboBox->clear();
    for(int i = 0; i < stations.size(); i++)
        ui->comboBox->addItem(stations[i].name);
    if(stations.size())
    {
        ui->lineEdit->setText(stations[0].name);
        ui->lineEdit_2->setText(QString::number(stations[0].address));
    }
}
