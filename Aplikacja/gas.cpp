#include "gas.h"


Gas::Gas(QWidget *parent) : QWidget(parent)
{
    val_text = new QLabel(this);
    QFont *font = new QFont("DejaVu Sans");
    font->setBold(true);
    font->setPixelSize(24);
    val_text->setFont(*font);

    p_meter = new smoke(60, 200, this);
    p_meter->setPixDiff(1);
    p_meter->setRoundingRadius(3);

    p_scale = new smoke_scale(30, 200, new QFont("times", 16), this);

    m_hbox.addWidget(val_text);
    m_hbox.addWidget(p_meter);
    m_hbox.addWidget(p_scale);
    setLayout(&m_hbox);
    setMinimumSize(QSize(50, 220));
}


//! Function that specify the percentage of chart fill.
/*!
\param value of fill in bar chart.
*/
void Gas::setValue(int val)
{
    val_text->setText(QString::number(val)+"ppm");
    val=val*170/1000;
    val = 170-val;
    val+=30;
    p_meter->setVUPos(val);
}
