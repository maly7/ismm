#ifndef STATION_TAB_H
#define STATION_TAB_H

#include <QWidget>
#include <QGroupBox>
#include <QGridLayout>
#include <QTimer>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSerialPort>
#include "temperature.h"
#include "humidity.h"
#include "movement.h"
#include "gas.h"

/**
 * \brief Klasa dziedzicząca po QWidget odpowiadająca za wyświetlanie zebranych pomiarów przez daną stację.
 * \author Marcin Węgrzyn
 */
class Station_Tab : public QWidget
{
    Q_OBJECT
public:
    explicit Station_Tab(QWidget *parent);
    QString station_name;
    int station_id;
    int station_address;
    bool station_active;
signals:

public slots:
    void getData(QSerialPort *srl);
    bool addData(float temp, int hum, int gas, bool move);
    void checkTimeout();
private:
    /**
     * \brief GroupBox w którym wyświetlane jest temperatura
     * \author Marcin Węgrzyn
     */
    QGroupBox *temp_box;
    /**
     * \brief GroupBox w którym wyświetlane jest wilgotność
     * \author Marcin Węgrzyn
     */
    QGroupBox *hum_box;
    /**
     * \brief GroupBox w którym wyświetlane jest ruch
     * \author Marcin Węgrzyn
     */
    QGroupBox *move_box;
    /**
     * \brief GroupBox w którym wyświetlane jest zadymienie
     * \author Marcin Węgrzyn
     */
    QGroupBox *gas_box;

    /**
     * \brief Wyświetlanie elementu odpowiedzialnego za wyświetlanie informacji o temperaturze.
     * \author Marcin Węgrzyn
     */
    Humidity *humidity;
    /**
     * \brief Wyświetlanie elementu odpowiedzialnego za wyświetlanie informacji o wilgotności.
     * \author Marcin Węgrzyn
     */
    Temperature *temperature;
    /**
     * \brief Wyświetlanie elementu odpowiedzialnego za wyświetlanie informacji o ruchu.
     * \author Marcin Węgrzyn
     */
    Movement *movement;
    /**
     * \brief Wyświetlanie elementu odpowiedzialnego za wyświetlanie informacji o zadymieniu.
     * \author Marcin Węgrzyn
     */
    Gas *gas;
    QTimer *tim_timeout;

    QSerialPort *serial;
};

#endif // STATION_TAB_H
