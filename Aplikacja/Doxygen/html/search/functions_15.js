var searchData=
[
  ['validrange',['validRange',['../classQCPRange.html#ab38bd4841c77c7bb86c9eea0f142dcc0',1,'QCPRange::validRange(double lower, double upper)'],['../classQCPRange.html#a801b964752eaad6219be9d8a651ec2b3',1,'QCPRange::validRange(const QCPRange &amp;range)']]],
  ['valuerange',['valueRange',['../classQCPDataContainer.html#a35a102dc2424d1228fc374d9313efbe9',1,'QCPDataContainer::valueRange()'],['../classQCPGraphData.html#a5eb7d51a8cdccbc38b4f745ca0bdfed9',1,'QCPGraphData::valueRange()'],['../classQCPCurveData.html#a50b97325b0edd13f6cff590ede62eb3c',1,'QCPCurveData::valueRange()'],['../classQCPBarsData.html#a87e9dc820e4b306da4dbec1a993bec14',1,'QCPBarsData::valueRange()'],['../classQCPStatisticalBoxData.html#abe311ba7ba785215f791db23498f702f',1,'QCPStatisticalBoxData::valueRange()'],['../classQCPFinancialData.html#a419360904ff00521dd96d53f3d672849',1,'QCPFinancialData::valueRange()']]]
];
