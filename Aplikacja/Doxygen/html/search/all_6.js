var searchData=
[
  ['gas',['Gas',['../classGas.html',1,'Gas'],['../classGas.html#a353f2a599ec4ba3435d3c629c1856609',1,'Gas::Gas(QWidget *parent=nullptr)'],['../classGas.html#a9d6c26dbe1b4bb8c5a8580f8063997c2',1,'Gas::gas()'],['../classhistory.html#aa795cc0b523f610a63722d3397e12a53',1,'history::gas()'],['../classStation__Tab.html#a510225f201d607c6dc7e5ec6973c36c2',1,'Station_Tab::gas()']]],
  ['gas_2ecpp',['gas.cpp',['../gas_8cpp.html',1,'']]],
  ['gas_2eh',['gas.h',['../gas_8h.html',1,'']]],
  ['gas_5fbox',['gas_box',['../classStation__Tab.html#aa41572eac2cd2089dda90b6f873282ff',1,'Station_Tab']]],
  ['gas_5fplot',['gas_plot',['../classhistory.html#ae95b36db111d6a164d28ad02b7664e8b',1,'history']]],
  ['getdata',['getData',['../classhistory.html#a95d37b2e64e9b08fd2856400c28d9927',1,'history::getData()'],['../classISMM.html#a4a5023ef6b78996685bbb23c6752a3b0',1,'ISMM::getData()'],['../classStation__Tab.html#a77f2f08dbf33cc21dccce8706b03de6e',1,'Station_Tab::getData()']]],
  ['graphicview2',['graphicView2',['../classHumidity.html#a192dfada23b35b74cfc7a69502849684',1,'Humidity']]]
];
