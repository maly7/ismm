#ifndef ISMM_H
#define ISMM_H

#include <QMainWindow>
#include <QDebug>
#include <QTimer>
#include <QDateTime>
#include <QVector>
#include <QSerialPort>
#include "station_tab.h"
#include "ismm_set.h"
#include "history.h"

namespace Ui {
class ISMM;
}

/**
 * \brief Główna klasa aplikacji.
 * \author Marcin Węgrzyn
 *
 */
class ISMM : public QMainWindow
{
    Q_OBJECT

public:
    explicit ISMM(QWidget *parent = nullptr);
    ~ISMM();
    int stationCount;

private slots:
    void on_pushButton_3_clicked();
    void timeUpdate();
    void getData();
    void on_pushButton_clicked();
    void refreshStations();
    void wyswietl_info();
    void on_pushButton_4_clicked();

private:
    Ui::ISMM *ui;

    /**
     * \brief Zmienna odpowiadająca czasowi odświeżania stacji (w sekundach).
     * \author Marcin Węgrzyn
     */
    int refresh_time;
    /**
     * \brief Wektor przechowujący wszystkie zapisane stacje.
     * \author Marcin Węgrzyn
     */
    QVector <Station_Tab*> stacje;
    /**
     * \brief Timer za pomocą którego odświeżany jest zegar.
     * \author Marcin Węgrzyn
     */
    QTimer *date_timer;
    /**
     * \brief Timer który wywołuje pobranie danych ze stacji.
     * \author Marcin Węgrzyn
     */
    QTimer *refresh_timer;
    QSerialPort *serial;
};

#endif // ISMM_H
