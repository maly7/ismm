#include "ismm.h"
#include "ui_ismm.h"
#include <QFile>
#include <QSerialPortInfo>

ISMM::ISMM(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ISMM)
{
    ui->setupUi(this);
    srand(time(NULL));

    serial = new QSerialPort(this);

    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        qDebug() << "Wykryto " << info.description() << " na porcie " << info.portName();
        if(info.description() == "ISMM Master")
        {
            serial->setPort(info);
            if(serial->open(QIODevice::ReadWrite))
            {
                serial->setBaudRate(QSerialPort::Baud115200);
                serial->setDataBits(QSerialPort::Data8);
                serial->setParity(QSerialPort::NoParity);
                serial->setStopBits(QSerialPort::OneStop);
                serial->setFlowControl(QSerialPort::NoFlowControl);
            }
            else
                qDebug() << serial->errorString();
        }
    }

    QSqlDatabase bdb = QSqlDatabase::addDatabase("QSQLITE");

    connect(ui->actionHistoria, SIGNAL(triggered(bool)), this, SLOT(on_pushButton_4_clicked()));
    connect(ui->actionUstawienia, SIGNAL(triggered(bool)), this, SLOT(on_pushButton_3_clicked()));
    connect(ui->actionWyj_cie, SIGNAL(triggered(bool)), this, SLOT(on_pushButton_clicked()));
    connect(ui->actionInformacje, SIGNAL(triggered(bool)), this, SLOT(wyswietl_info()));

    date_timer = new QTimer(this);
    connect(date_timer, SIGNAL(timeout()), this, SLOT(timeUpdate()));
    timeUpdate();

    refresh_time = 2;
    
    refresh_timer = new QTimer(this);
    connect(refresh_timer, SIGNAL(timeout()), this, SLOT(getData()));
    refreshStations();
    //getData();
}

ISMM::~ISMM()
{
    delete ui;
}

/**
 * \brief Zleca pobranie danych pomiarowych z każdej stacji.
 * \author Marcin Węgrzyn
 *
 * Metoda wykonywana cyklicznie co określony czas. Wywołuje metodę getData klasy Station_Tab.
 *
 */
void ISMM::getData()
{
    if(!serial->isDataTerminalReady())
    {
        if(serial->isOpen())
            serial->close();
        else
        {
            foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
            {
                qDebug() << "Wykryto " << info.description() << " na porcie " << info.portName();
                if(info.description() == "ISMM Master")
                {
                    serial->setPort(info);
                    if(serial->open(QIODevice::ReadWrite))
                    {
                        serial->setBaudRate(QSerialPort::Baud115200);
                        serial->setDataBits(QSerialPort::Data8);
                        serial->setParity(QSerialPort::NoParity);
                        serial->setStopBits(QSerialPort::OneStop);
                        serial->setFlowControl(QSerialPort::NoFlowControl);
                    }
                    else
                        qDebug() << serial->errorString();
                }
            }
        }
    }

    for(int i = 0; i < stacje.size(); i++)
    {
        stacje[i]->getData(serial);
        if(!serial->isOpen() || !stacje[i]->station_active)
            stacje[i]->setEnabled(false);
        else
            stacje[i]->setEnabled(true);
    }

    if(serial->isOpen())
        refresh_timer->start(1000*refresh_time);
    else
        refresh_timer->start(1000);
}

/**
 * \brief Pobiera informacje o każdej stacji pomiarowe.
 * \author Marcin Węgrzyn
 *
 * Metoda wywoływana w konstruktorze oraz po każdej edycji ustawień stacji.
 *
 */
void ISMM::refreshStations()
{
    QSqlDatabase bdb = QSqlDatabase::database();
    QSqlQuery *query;
    bdb.setDatabaseName("./database.db");
    query = new QSqlQuery(bdb);
    if(bdb.open())
    {
        stacje.clear();
        ui->tabWidget->clear();
        query->exec("SELECT * FROM Stations");
        while (query->next())
        {
            stacje.push_back(new Station_Tab(this));
            stacje.last()->station_id = query->value(0).toInt();
            stacje.last()->station_address = query->value(2).toInt();
            stacje.last()->station_name = query->value(1).toString();
            ui->tabWidget->addTab(stacje.last(), stacje.last()->station_name);
        }
        bdb.close();
    }
    if(stacje.size() == 0) ui->pushButton_4->setEnabled(false);
    else ui->pushButton_4->setEnabled(true);
    refresh_timer->stop();
    getData();
}

/**
 * \brief Metoda pobierająca aktualną datę i czas.
 * \author Marcin Węgrzyn
 *
 */
void ISMM::timeUpdate()
{
    QTime time = QDateTime::currentDateTime().time();
    QDate data = QDateTime::currentDateTime().date();

    QString data_txt;
    if(data.day()<10) data_txt.append('0');
    data_txt.append(QString::number(data.day()));
    data_txt.append('.');
    if(data.month()<10) data_txt.append('0');
    data_txt.append(QString::number(data.month()));
    data_txt.append('.');
    data_txt.append(QString::number(data.year()));
    data_txt.append("r.");

    ui->lbl_Date->setText(data_txt);

    QString time_txt;
    if(time.hour()<10) time_txt.append('0');
    time_txt.append(QString::number(time.hour()));
    time_txt.append(':');
    if(time.minute()<10) time_txt.append('0');
    time_txt.append(QString::number(time.minute()));


    ui->lbl_Time->setText(time_txt);
    date_timer->start(20000);
}

void ISMM::wyswietl_info()
{
    QMessageBox::about(this, "Informacje", "Aplikacja <b>Inteligentny System Monitorowania Mieszkania</b><br><br>"
                                           "Projekt: Wizualizacje Danych Sensorycznych<br>"
                                           "Prowadzący dr inż. Bogdan Kreczmer<br><br>"
                                           "<b>Autorzy:</b><br>Marcin Węgrzyn<br>Karol Oleśkiewicz");
}

void ISMM::on_pushButton_3_clicked()
{
    ISMM_SET settings(this);
    connect(&settings, SIGNAL(exitValue()), this, SLOT(refreshStations()));
    settings.exec();
}

void ISMM::on_pushButton_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Wyjście!", "Czy napewno chcesz zamknąć aplikację?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
        this->close();
}

void ISMM::on_pushButton_4_clicked()
{
    history hist(this, stacje[ui->tabWidget->currentIndex()]->station_id, stacje[ui->tabWidget->currentIndex()]->station_name);
    hist.exec();
}
