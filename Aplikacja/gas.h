#ifndef GAS_H
#define GAS_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QFont>
#include "smoke.h"
#include "smoke_scale.h"

//!  Smoke widget class. 
/*!
  Gas is a parent of QWidget.
*/
class Gas : public QWidget
{
    Q_OBJECT
public:

    //! A constructor.
    /*!
      Constructor of Gas, QWidget is his parent.
    */
    explicit Gas(QWidget *parent = nullptr);
    float value;

signals:

public slots:
    void setValue(int val);
private:
    smoke *gas;
    smoke* p_meter;
    smoke_scale* p_scale;
    QHBoxLayout m_hbox;
    QLabel *val_text;
};

#endif // GAS_H
