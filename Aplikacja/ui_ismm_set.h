/********************************************************************************
** Form generated from reading UI file 'ismm_set.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ISMM_SET_H
#define UI_ISMM_SET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_ISMM_SET
{
public:
    QLabel *label;
    QSpinBox *spin_Freq;
    QGroupBox *groupBox;
    QComboBox *comboBox;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLabel *label_3;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;

    void setupUi(QDialog *ISMM_SET)
    {
        if (ISMM_SET->objectName().isEmpty())
            ISMM_SET->setObjectName(QStringLiteral("ISMM_SET"));
        ISMM_SET->resize(400, 190);
        ISMM_SET->setMinimumSize(QSize(400, 190));
        ISMM_SET->setMaximumSize(QSize(400, 190));
        label = new QLabel(ISMM_SET);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 231, 25));
        spin_Freq = new QSpinBox(ISMM_SET);
        spin_Freq->setObjectName(QStringLiteral("spin_Freq"));
        spin_Freq->setGeometry(QRect(290, 10, 101, 25));
        spin_Freq->setMinimum(1);
        groupBox = new QGroupBox(ISMM_SET);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(20, 50, 361, 101));
        groupBox->setFlat(false);
        groupBox->setCheckable(false);
        comboBox = new QComboBox(groupBox);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(50, 0, 191, 20));
        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(240, 0, 61, 20));
        pushButton_2 = new QPushButton(groupBox);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(300, 0, 61, 20));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 30, 67, 25));
        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(120, 30, 231, 25));
        lineEdit_2 = new QLineEdit(groupBox);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(120, 60, 231, 25));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 60, 67, 25));
        pushButton_3 = new QPushButton(ISMM_SET);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(290, 160, 89, 25));
        pushButton_4 = new QPushButton(ISMM_SET);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(190, 160, 89, 25));

        retranslateUi(ISMM_SET);

        QMetaObject::connectSlotsByName(ISMM_SET);
    } // setupUi

    void retranslateUi(QDialog *ISMM_SET)
    {
        ISMM_SET->setWindowTitle(QApplication::translate("ISMM_SET", "Ustawienia", 0));
        label->setText(QApplication::translate("ISMM_SET", "Cz\304\231stotliwo\305\233\304\207 pomiar\303\263w (min):", 0));
        groupBox->setTitle(QApplication::translate("ISMM_SET", "Edycja", 0));
        pushButton->setText(QApplication::translate("ISMM_SET", "Dodaj", 0));
        pushButton_2->setText(QApplication::translate("ISMM_SET", "Usu\305\204", 0));
        label_2->setText(QApplication::translate("ISMM_SET", "Nazwa:", 0));
        label_3->setText(QApplication::translate("ISMM_SET", "Adres:", 0));
        pushButton_3->setText(QApplication::translate("ISMM_SET", "Zapisz", 0));
        pushButton_4->setText(QApplication::translate("ISMM_SET", "Anuluj", 0));
    } // retranslateUi

};

namespace Ui {
    class ISMM_SET: public Ui_ISMM_SET {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ISMM_SET_H
