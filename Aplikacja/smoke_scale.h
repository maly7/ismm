#ifndef SMOKE_SCALE_H
#define SMOKE_SCALE_H

#include <QWidget>
//!  Test class of work
/*!
  Test, temporary class for app working
  /author Karol Oleskiewicz
*/
class Test;

//!  Scale to smoke
/*!
  This class contains scale and adds it to the chart.
  /author Karol Oleskiewicz
*/
class smoke_scale : public QWidget
{
public:
    //! A constructor of scale.
    /*!
      This functions generates the scale addon.
    */
    explicit smoke_scale(int w, int h, QFont* font, QWidget* parent=0);

    //! A deconstructor of scale.
    /*!
         Candid deconstructor.
    */
    virtual ~smoke_scale();

    void paintEvent(QPaintEvent* ev);

private:
    //! Draw base line of chart.
    /*!
        \param x1 needed to movement of chart.
        \param x2 needed to generate the lines.
        \param color defines a color of chart.
        \param pnt pointer to qpainter.
        \param path Path draw a path to setpoint.
    */
    void _drawBaseLine(QPoint x1, QPoint x2, QColor color, QPainter * const pnt, QPainterPath* path);
    //! Draw Ruler close to chart.
    /*!
        \param step Specify the step of ruller.
        \param color defines a color of chart.
        \param pnt pointer to qpainter.
        \param path Path draw a path to setpoint.
    */
    void _drawRuler(int step, QColor color, QPainter* const pnt, QPainterPath *path);
    //! Writes text close to chart.
    /*!
        \param pos Position for drawing a text.
        \param txt Specify the position of text on the chart.
        \param pnt pointer to qpainter.
        \param path Path draw a path to setpoint.
    */
    void _drawText(QPoint pos, QString txt, QPainter* const pnt, QPainterPath* path);
private:
    /** 
    * a private variable.
    * Set the width of scale.
    */
    int m_width;
    /** 
    * a private variable.
    * Set the height of scale
    */
    int m_height;
    /** 
    * a private variable.
    * It defines a font for chart.
    */
    QFont* p_font;

    friend class Test;

};

#endif // SMOKE_SCALE_H
