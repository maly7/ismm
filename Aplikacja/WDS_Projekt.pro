#-------------------------------------------------
#
# Project created by QtCreator 2017-05-11T18:22:11
#
#-------------------------------------------------

QT       += core gui sql printsupport serialport
CONFIG += c++11


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WDS_Projekt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        ismm.cpp \
    ismm_set.cpp \
    station_tab.cpp \
    temperature.cpp \
    humidity.cpp \
    gas.cpp \
    movement.cpp \
    history.cpp \
    qcustomplot.cpp \
    smoke.cpp \
    smoke_scale.cpp

HEADERS  += ismm.h \
    ismm_set.h \
    station_tab.h \
    temperature.h \
    humidity.h \
    gas.h \
    movement.h \
    history.h \
    qcustomplot.h \
    smoke.h \
    smoke_scale.h

FORMS    += ismm.ui \
    ismm_set.ui \
    history.ui

RESOURCES += \
    zasoby.qrc

STATECHARTS +=
