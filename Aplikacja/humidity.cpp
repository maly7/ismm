#include "humidity.h"

#include <QDebug>
Humidity::Humidity(QWidget *parent) : QWidget(parent)
{
    QGraphicsView *graphicView = new QGraphicsView(this);
    QGraphicsScene *scene =  new QGraphicsScene(graphicView);
    scene->addPixmap(QPixmap(":/images/kropla.png"));
    graphicView->setScene(scene);
    graphicView->scale(0.8, 0.8);
    graphicView->setAlignment(Qt::AlignTop|Qt::AlignLeft);
    graphicView->setStyleSheet("background: transparent; border: 0px;");
    graphicView->setMinimumSize(150, 150);

    graphicView2 = new QGraphicsView(graphicView);
    scene2 =  new QGraphicsScene(graphicView2);
    scene2->addPixmap(QPixmap(":/images/kropla_w.png"));
    graphicView2->setScene(scene2);
    graphicView2->setAlignment(Qt::AlignTop|Qt::AlignLeft);
    graphicView2->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    graphicView2->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    graphicView2->scale(0.75, 0.75);
    graphicView2->setStyleSheet("background: transparent; border: 0px;");

    QFont *font = new QFont("DejaVu Sans");
    value_txt = new QLabel(graphicView);
    value_txt->raise();
    font->setBold(true);
    font->setPixelSize(30);
    value_txt->setAlignment(Qt::AlignCenter);
    value_txt->setFont(*font);
    value_txt->setGeometry(33, 90, 80, 40);
    value_txt->setStyleSheet("color: #CDCDCD; border: 2px solid #CDCDCD; border-radius: 15px; background-color: #FFFFFF");

    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addWidget(graphicView);
    hbox->setAlignment(graphicView, Qt::AlignCenter);
    this->setLayout(hbox);

    setValue(0);
}

/**
 * \brief Metoda odpowiadająca za ustawienie odpowiedniego poziomu wilgotności.
 *
 * \author Marcin Węgrzyn
 *
 * Metoda ustawia odpowiednio wypełnienie kropli oraz edytuje tekst informujący o % wilgotności powietrza.
 *
 * \param[in] val Wartość procentowa informująca o poziomie wilgotności powietrza.
 */
void Humidity::setValue(int val)
{
    int wysokosc = val*180/100;
    value_txt->setText(tr("%1%").arg(val));
    scene2->setSceneRect(0, 0, 0, wysokosc);
    graphicView2->setGeometry(4, 6, 150, 180-wysokosc);
}
