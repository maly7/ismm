#include "history.h"
#include "ui_history.h"

history::history(QWidget *parent, int station_id, QString station_name) :
    QDialog(parent),
    ui(new Ui::history)
{
    ui->setupUi(this);

    ui->label->setText("Historia pomiarów dla stacji pomiarowej " + station_name + " ("+QString::number(station_id)+"):");
    this->setWindowTitle("Historia ("+station_name+")");

    //QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
    //dateTicker->setDateTimeFormat("hh:mm\ndd.MM");
    temp_plot = new QCustomPlot(this);
    temp_plot->addGraph();
    temp_plot->plotLayout()->insertRow(0);
    temp_plot->plotLayout()->addElement(0, 0, new QCPTextElement(temp_plot, "Pomiary temperatury w czasie", QFont("sans", 10, QFont::Normal)));
    temp_plot->xAxis->setLabel("Pomiary");
    //temp_plot->xAxis->setTicker(dateTicker);
    temp_plot->yAxis->setLabel("Temperatura");
    temp_plot->yAxis->setRange(15, 30);
    temp_plot->replot();

    hum_plot = new QCustomPlot(this);
    hum_plot->plotLayout()->insertRow(0);
    hum_plot->plotLayout()->addElement(0, 0, new QCPTextElement(hum_plot, "Pomiary wilgotności powietrza w czasie", QFont("sans", 10, QFont::Normal)));
    hum_plot->xAxis->setLabel("Pomiary");
    //hum_plot->xAxis->setTicker(dateTicker);
    hum_plot->yAxis->setLabel("Wilgotność powietrza");
    hum_plot->yAxis->setRange(0, 100);
    hum_plot->replot();

    gas_plot = new QCustomPlot(this);
    gas_plot->plotLayout()->insertRow(0);
    gas_plot->plotLayout()->addElement(0, 0, new QCPTextElement(gas_plot, "Pomiary zadymienia w czasie", QFont("sans", 10, QFont::Normal)));
    gas_plot->xAxis->setLabel("Pomiary");
    //gas_plot->xAxis->setTicker(dateTicker);
    gas_plot->yAxis->setLabel("Zadymienie");
    gas_plot->yAxis->setRange(0, 1000);
    gas_plot->replot();

    move_plot = new QCustomPlot(this);
    move_plot->plotLayout()->insertRow(0);
    move_plot->plotLayout()->addElement(0, 0, new QCPTextElement(move_plot, "Pomiary wykrycia ruchu w czasie", QFont("sans", 10, QFont::Normal)));
    move_plot->xAxis->setLabel("Pomiary");
    //move_plot->xAxis->setTicker(dateTicker);
    move_plot->yAxis->setLabel("Ruch");
    move_plot->yAxis->setRange(0, 1);
    move_plot->replot();

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(ui->label);
    vbox->addWidget(temp_plot);
    vbox->addWidget(hum_plot);
    vbox->addWidget(gas_plot);
    vbox->addWidget(move_plot);
    this->setLayout(vbox);

    getData(station_id);
}

/**
 * \brief Metoda odpowiadająca za pobieranie danych z bazy danych oraz wstawianie ich do odpowiednich wykresów.
 *
 * \author Marcin Węgrzyn
 *
 * \param[in] station_id ID Stacji której pomiary ma pobierać.
 */
void history::getData(int station_id)
{
    QSqlDatabase bdb = QSqlDatabase::database();
    QSqlQuery *query;
    bdb.setDatabaseName("./database.db");
    query = new QSqlQuery(bdb);
    int i = 0;
    if(bdb.open())
    {
        query->exec("SELECT * FROM Measurements WHERE Station = "+QString::number(station_id)+" ORDER BY Datet ASC");
        while (query->next())
        {
            ids.push_back(i++);
            time.push_back(query->value(1).toDateTime());
            temp.push_back(query->value(2).toFloat());
            hum.push_back(query->value(3).toInt());
            gas.push_back(query->value(4).toInt());
            move.push_back(query->value(5).toBool());
        }
        bdb.close();
    }
    temp_plot->addGraph();
    temp_plot->graph(0)->setData(ids, temp);
    temp_plot->xAxis->setRange(0, temp.size());
    temp_plot->replot();
    hum_plot->addGraph();
    hum_plot->graph(0)->setData(ids, hum);
    hum_plot->xAxis->setRange(0, hum.size());
    hum_plot->replot();
    gas_plot->addGraph();
    gas_plot->graph(0)->setData(ids, gas);
    gas_plot->xAxis->setRange(0, gas.size());
    gas_plot->replot();
    move_plot->addGraph();
    move_plot->graph(0)->setData(ids, move);
    move_plot->xAxis->setRange(0, move.size());
    move_plot->replot();
}

history::~history()
{
    delete ui;
}
