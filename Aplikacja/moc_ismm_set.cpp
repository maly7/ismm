/****************************************************************************
** Meta object code from reading C++ file 'ismm_set.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "ismm_set.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ismm_set.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ISMM_SET_t {
    QByteArrayData data[13];
    char stringdata0[220];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ISMM_SET_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ISMM_SET_t qt_meta_stringdata_ISMM_SET = {
    {
QT_MOC_LITERAL(0, 0, 8), // "ISMM_SET"
QT_MOC_LITERAL(1, 9, 9), // "exitValue"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(4, 44, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(5, 68, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(6, 90, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(7, 114, 23), // "on_lineEdit_textChanged"
QT_MOC_LITERAL(8, 138, 4), // "arg1"
QT_MOC_LITERAL(9, 143, 25), // "on_lineEdit_2_textChanged"
QT_MOC_LITERAL(10, 169, 31), // "on_comboBox_currentIndexChanged"
QT_MOC_LITERAL(11, 201, 5), // "index"
QT_MOC_LITERAL(12, 207, 12) // "refreshCombo"

    },
    "ISMM_SET\0exitValue\0\0on_pushButton_4_clicked\0"
    "on_pushButton_3_clicked\0on_pushButton_clicked\0"
    "on_pushButton_2_clicked\0on_lineEdit_textChanged\0"
    "arg1\0on_lineEdit_2_textChanged\0"
    "on_comboBox_currentIndexChanged\0index\0"
    "refreshCombo"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ISMM_SET[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   60,    2, 0x08 /* Private */,
       4,    0,   61,    2, 0x08 /* Private */,
       5,    0,   62,    2, 0x08 /* Private */,
       6,    0,   63,    2, 0x08 /* Private */,
       7,    1,   64,    2, 0x08 /* Private */,
       9,    1,   67,    2, 0x08 /* Private */,
      10,    1,   70,    2, 0x08 /* Private */,
      12,    0,   73,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void,

       0        // eod
};

void ISMM_SET::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ISMM_SET *_t = static_cast<ISMM_SET *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->exitValue(); break;
        case 1: _t->on_pushButton_4_clicked(); break;
        case 2: _t->on_pushButton_3_clicked(); break;
        case 3: _t->on_pushButton_clicked(); break;
        case 4: _t->on_pushButton_2_clicked(); break;
        case 5: _t->on_lineEdit_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_lineEdit_2_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_comboBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->refreshCombo(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ISMM_SET::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ISMM_SET::exitValue)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject ISMM_SET::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ISMM_SET.data,
      qt_meta_data_ISMM_SET,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ISMM_SET::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ISMM_SET::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ISMM_SET.stringdata0))
        return static_cast<void*>(const_cast< ISMM_SET*>(this));
    return QDialog::qt_metacast(_clname);
}

int ISMM_SET::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void ISMM_SET::exitValue()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
