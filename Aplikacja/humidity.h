#ifndef HUMIDITY_H
#define HUMIDITY_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QHBoxLayout>
#include <QTimer>
#include <QPixmap>
#include <QLabel>
#include <QFont>

/**
 * \brief Klasa dziedzicząca po QWidget odpowiadająca za wyświetlanie wilgotności powietrza.
 * \author Marcin Węgrzyn
 */
class Humidity : public QWidget
{
    Q_OBJECT
public:
    explicit Humidity(QWidget *parent = nullptr);
signals:

public slots:
    void setValue(int value);
private:
    /**
     * \brief Odpowiada za element wyświetlający białą krople na tle niebieskiej.
     * \author Marcin Węgrzyn
     * Biała kropla jest wyrównywana do góry kropli niebieskiej oraz ucinana od dołu co odpowiada odpowiedniemu poziomowi wilgotności.
     */
    QGraphicsView *graphicView2;
    QGraphicsScene *scene2;
    /**
     * \brief QLabel odpowiadający za wyświetlanie wartości wilgotności powietrza.
     * \author Marcin Węgrzyn
     */
    QLabel *value_txt;
};

#endif // HUMIDITY_H
