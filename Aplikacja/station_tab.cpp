#include "station_tab.h"


Station_Tab::Station_Tab(QWidget *parent = 0) : QWidget(parent)
{
    station_name = "NowaStacja";

    QGridLayout *grid = new QGridLayout;

    this->setStyleSheet("QGroupBox {background-color: #EEEEEE;}");
    
    temp_box = new QGroupBox("Temperatura", this);
    hum_box = new QGroupBox("Wilgotność powietrza", this);
    gas_box = new QGroupBox("Zadymienie", this);
    move_box = new QGroupBox("Ruch", this);

    QGridLayout *temp_grid = new QGridLayout(temp_box);
    QGridLayout *hum_grid = new QGridLayout(hum_box);
    QGridLayout *gas_grid = new QGridLayout(gas_box);
    QGridLayout *move_grid = new QGridLayout(move_box);

    temperature = new Temperature(this);
    humidity = new Humidity(this);
    movement = new Movement(this);
    gas = new Gas(this);

    grid->addWidget(temp_box, 0, 0);
    grid->addWidget(hum_box, 0, 1);
    grid->addWidget(gas_box, 1, 0);
    grid->addWidget(move_box, 1, 1);

    temp_grid->addWidget(temperature, 0 , 0);
    hum_grid->addWidget(humidity, 0 , 0);
    move_grid->addWidget(movement, 0 , 0);
    gas_grid->addWidget(gas, 0 , 0);

    setLayout(grid);
}

/**
 * \brief Metoda odpowiedzialna za wyświetlenie pobranych danych.
 *
 * \author Marcin Węgrzyn
 *
 * Metoda pobiera dane ze stacji pogodowej oraz wywołuje metody setValue poszczególnych klass (Humidity, Temperature, Gas, Movement).
 */
void Station_Tab::getData(QSerialPort *srl)
{
    serial = srl;
    if(serial->isOpen())
    {
        serial->waitForBytesWritten(1000);
        QString cmd = QString("APP:")+QString::number(station_address)+QString(";0;")+QString::number(station_address%9)+QString(":END\r\n");
        serial->write(cmd.toStdString().c_str());
        serial->waitForReadyRead(1000);
        QString response = serial->readAll();
        qDebug() << response;
        if(response.size() > 5)
        {
            int ids, hum, gasv, crc;
            bool move;
            int temp, temp2;
            sscanf(response.toStdString().c_str(), "MSTR;%d;%d;%d;%d;%d;%d;%d:END\n", &ids, &temp, &temp2, &hum, &gasv, (int*)&move, &crc);
            if((temp+temp2+hum+2+gasv+move)%36==crc)
            {
                station_active = true;
                float ftemp = 1.0*temp+temp2/10.0;
                qDebug() << "ID: " << ids;
                qDebug() << "Temp: " << ftemp;
                qDebug() << "Hum: " << hum;
                qDebug() << "Gas: " << gasv;
                qDebug() << "Move: " << move;
                qDebug() << "CRC: " << crc;
                temperature->setValue(ftemp);
                humidity->setValue(hum);
                movement->addMove(move);
                gas->setValue(gasv);
                addData(temp, hum, gasv, move);
            }
            else
                station_active = false;
        }
    }
}

void Station_Tab::checkTimeout()
{

}

/**
 * \brief Metoda odpowiedzialna za wstawienie pobranych danych do bazy danych.
 *
 * \author Marcin Węgrzyn
 *
 * \param[in] temp Wartość temperatury (15-30).
 * \param[in] hum Wartość wilgotności powietrza (0-100).
 * \param[in] gas Wartość zadymienia pomieszczenia (0-1000).
 * \param[in] move Wartość odpowiadająca za wykryty ruch (true/false).
 */
bool Station_Tab::addData(float temp, int hum, int gas, bool move)
{
  QSqlDatabase bdb = QSqlDatabase::database();
  QSqlQuery *query;
  bdb.setDatabaseName("database.db");
  query = new QSqlQuery();
  if(bdb.open())
  {
    query->exec("INSERT INTO Measurements VALUES ("+QString::number(station_id)+", CURRENT_TIMESTAMP, "+QString::number(temp)+", "+QString::number(hum)+", "+QString::number(gas)+", "+QString::number(move)+");");
    bdb.close();
    return true;
  }
  return false;
}
