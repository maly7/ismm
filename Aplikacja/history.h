#ifndef HISTORY_H
#define HISTORY_H

#include <QDialog>
#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVector>
#include <QVBoxLayout>
#include "qcustomplot.h"


namespace Ui {
class history;
}

/**
 * \brief Klasa odpowiadająca za wyświetlanie historii pomiarów (tworząca wykresy).
 * \author Marcin Węgrzyn
 */
class history : public QDialog
{
    Q_OBJECT

public:
    explicit history(QWidget *parent = nullptr, int station_id = -1, QString station_name = "Unknown");
    ~history();

private:
    Ui::history *ui;
    void getData(int station_id);
    /**
     * \brief Wykres temperatury.
     * \author Marcin Węgrzyn
     */
    QCustomPlot *temp_plot;
    /**
     * \brief Wykres wilgotności.
     * \author Marcin Węgrzyn
     */
    QCustomPlot *hum_plot;
    /**
     * \brief Wykres zadymienia.
     * \author Marcin Węgrzyn
     */
    QCustomPlot *gas_plot;
    /**
     * \brief Wykres ruchu.
     * \author Marcin Węgrzyn
     */
    QCustomPlot *move_plot;

    QVector <double> ids;
    QVector <QDateTime> time;
    QVector <double> temp;
    QVector <double> hum;
    QVector <double> gas;
    QVector <double> move;
};

#endif // HISTORY_H
