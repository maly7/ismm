/****************************************************************************
** Meta object code from reading C++ file 'station_tab.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "station_tab.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'station_tab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Station_Tab_t {
    QByteArrayData data[11];
    char stringdata0[77];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Station_Tab_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Station_Tab_t qt_meta_stringdata_Station_Tab = {
    {
QT_MOC_LITERAL(0, 0, 11), // "Station_Tab"
QT_MOC_LITERAL(1, 12, 7), // "getData"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 12), // "QSerialPort*"
QT_MOC_LITERAL(4, 34, 3), // "srl"
QT_MOC_LITERAL(5, 38, 7), // "addData"
QT_MOC_LITERAL(6, 46, 4), // "temp"
QT_MOC_LITERAL(7, 51, 3), // "hum"
QT_MOC_LITERAL(8, 55, 3), // "gas"
QT_MOC_LITERAL(9, 59, 4), // "move"
QT_MOC_LITERAL(10, 64, 12) // "checkTimeout"

    },
    "Station_Tab\0getData\0\0QSerialPort*\0srl\0"
    "addData\0temp\0hum\0gas\0move\0checkTimeout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Station_Tab[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x0a /* Public */,
       5,    4,   32,    2, 0x0a /* Public */,
      10,    0,   41,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Bool, QMetaType::Float, QMetaType::Int, QMetaType::Int, QMetaType::Bool,    6,    7,    8,    9,
    QMetaType::Void,

       0        // eod
};

void Station_Tab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Station_Tab *_t = static_cast<Station_Tab *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->getData((*reinterpret_cast< QSerialPort*(*)>(_a[1]))); break;
        case 1: { bool _r = _t->addData((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: _t->checkTimeout(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QSerialPort* >(); break;
            }
            break;
        }
    }
}

const QMetaObject Station_Tab::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Station_Tab.data,
      qt_meta_data_Station_Tab,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Station_Tab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Station_Tab::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Station_Tab.stringdata0))
        return static_cast<void*>(const_cast< Station_Tab*>(this));
    return QWidget::qt_metacast(_clname);
}

int Station_Tab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
