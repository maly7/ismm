#ifndef SMOKE_H
#define SMOKE_H

#include <QWidget>

class QPainter;
class QPathPainter;
class Test;

//!  Smoke chart
/*!
  This class contains smoke chart.
  /author Karol Oleskiewicz
*/
class smoke : public QWidget
{
public:
    explicit smoke(int width, int height, QWidget* parent=nullptr);
    virtual ~smoke();
    void setPixDiff(unsigned int pdiff);
    void setRoundingRadius(qreal r);
    void paintEvent(QPaintEvent* ev);

    void setPeakPos(int pos);
    void setRMSPost(int pos);
    void setVUPos(int pos);

private:

    void _fillFromTo(QPoint p1, QPoint p2, QColor color, QPainter* const pnt, QPainterPath* const path);
    void _drawLine(QPoint p1, QColor color, QPainter* const pnt, QPainterPath  *path);
    void _drawBorders(QColor color, QRect rect, QPainter* const pnt);

private:
    int m_fillWidth;
    int m_totalWidth;
    int m_fillHeight;
    int m_totalHeight;
    qreal m_roundRad;
    unsigned int m_pixDiff;

    struct {
        int peak;
        int rms;
        int vu;
    } m_positions;

    friend class Test;
};


#endif // SMOKE_H
