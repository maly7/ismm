#ifndef _DELAY_H_
#define _DELAY_H_

#include "stm32f0xx_hal.h"
#include "tim.h"

void Delay_Init(TIM_TypeDef *timer);
void Delay(unsigned int time);

#endif
