#ifndef _DS18B20_
#define _DS18B20_

#include "stm32f0xx_hal.h"
#include "onewire.h"

#define DS18B20_Resolution_9bits	9  	/*!< DS18B20 9 bits resolution */
#define DS18B20_Resolution_10bits 	10	/*!< DS18B20 10 bits resolution */
#define DS18B20_Resolution_11bits	11	/*!< DS18B20 11 bits resolution */
#define DS18B20_Resolution_12bits	12 	/*!< DS18B20 12 bits resolution */

#define DS18B20_FAMILY_CODE				0x28

#define DS18B20_CMD_CONVERTTEMP			0x44 	/* Convert temperature */
#define DS18B20_DECIMAL_STEPS_12BIT		0.0625
#define DS18B20_DECIMAL_STEPS_11BIT		0.125
#define DS18B20_DECIMAL_STEPS_10BIT		0.25
#define DS18B20_DECIMAL_STEPS_9BIT		0.5

#define DS18B20_RESOLUTION_R1			6
#define DS18B20_RESOLUTION_R0			5

#ifdef DS18B20_USE_CRC
#define DS18B20_DATA_LEN				9
#else
#define DS18B20_DATA_LEN				2
#endif



uint8_t is_ds18b20(uint8_t *rom);
void ds18b20_start_all(OWire* owire);
uint8_t ds18b20_all_done(OWire* owire);
uint8_t ds18n20_start(OWire* owire, uint8_t *rom);
uint8_t ds18b20_read(OWire* owire, uint8_t *rom, float *destination);
uint8_t ds18b20_setresolution(OWire* owire, uint8_t *rom, uint8_t resolution);

#endif
