/*
 * dht.h
 *
 *  Created on: 07.05.2017
 *      Author: maly
 */

#ifndef DHT_H_
#define DHT_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"
#include "delay.h"

#define MAX_TICS 10000
#define DHT11_OK 0
#define DHT11_NO_CONN 1
#define DHT11_CS_ERROR 2


typedef struct{
  GPIO_TypeDef*         m_Port;
  __IO uint32_t*        m_Register;
  uint32_t              m_RegMask;
  uint32_t              m_IoMask;
  uint16_t              m_BitMask;
}DHT;


uint8_t DHT11_RawRead(DHT *dht, uint8_t *buf);
uint8_t DHT11_Humidity(uint8_t *buf);
uint8_t DHT11_Temperature(uint8_t *buf);
void DHT11_Init(DHT* dht, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void DHT11_WriteLow(DHT* dht);
void DHT11_WriteHigh(DHT* dht);
uint8_t DHT11_ReadPin(DHT* dht);
void DHT11_Output(DHT* dht);
void DHT11_Input(DHT* dht);

#endif /* DHT_H_ */
