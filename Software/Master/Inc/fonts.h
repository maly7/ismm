

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _USE_FONTS_H
#define _USE_FONTS_H


/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

extern const unsigned char font[];
extern const unsigned char klucz[];
extern const unsigned char info[];
extern const unsigned char dron[];
extern const unsigned char bateria[];
extern const unsigned char monitor[];
extern const unsigned char ladowanie_icon[];
extern const unsigned char kolo[];
extern const unsigned char zegar[];
extern const unsigned char wifi[];
extern const unsigned char up_arr[];
extern const unsigned char down_arr[];

#endif

/*-------------------------------END OF FILE-------------------------------*/

