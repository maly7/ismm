#ifndef __oled_H
#define __oled_H

#include "stm32f0xx_hal.h"

#define OLED_ADDRESS 0x78

#define OLED_LCDWIDTH 128
#define OLED_LCDHEIGHT 64

#define FONT_1206    12
#define FONT_1608    16

#define OLED_SETCONTRAST 0x81
#define OLED_DISPLAYALLON_RESUME 0xA4
#define OLED_DISPLAYALLON 0xA5
#define OLED_NORMALDISPLAY 0xA6
#define OLED_INVERTDISPLAY 0xA7
#define OLED_DISPLAYOFF 0xAE
#define OLED_DISPLAYON 0xAF

#define OLED_SETDISPLAYOFFSET 0xD3
#define OLED_SETCOMPINS 0xDA

#define OLED_SETVCOMDETECT 0xDB

#define OLED_SETDISPLAYCLOCKDIV 0xD5
#define OLED_SETPRECHARGE 0xD9

#define OLED_SETMULTIPLEX 0xA8

#define OLED_SETLOWCOLUMN 0x00
#define OLED_SETHIGHCOLUMN 0x10

#define OLED_SETSTARTLINE 0x40

#define OLED_MEMORYMODE 0x20
#define OLED_COLUMNADDR 0x21
#define OLED_PAGEADDR   0x22

#define OLED_COMSCANINC 0xC0
#define OLED_COMSCANDEC 0xC8

#define OLED_SEGREMAP 0xA0

#define OLED_CHARGEPUMP 0x8D

#define OLED_EXTERNALVCC 0x1
#define OLED_SWITCHCAPVCC 0x2

// Scrolling #defines
#define OLED_ACTIVATE_SCROLL 0x2F
#define OLED_DEACTIVATE_SCROLL 0x2E
#define OLED_SET_VERTICAL_SCROLL_AREA 0xA3
#define OLED_RIGHT_HORIZONTAL_SCROLL 0x26
#define OLED_LEFT_HORIZONTAL_SCROLL 0x27
#define OLED_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29
#define OLED_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL 0x2A


#define BLACK 0
#define WHITE 1
#define INVERSE 2

void oled_init(void);
void oled_display(void);
void oled_invert(void);
void oled_clear(void);
void oled_fill(uint8_t color);
void oled_pix(uint8_t x, uint8_t y, uint8_t color);

void oled_on(void);
void oled_off(void);
void oled_fcircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color);
void oled_circle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color);
void oled_ftriangle(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t x3, uint8_t y3, uint8_t color);
void oled_triangle(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t x3, uint8_t y3, uint8_t color);
void oled_frect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color);
void oled_rect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color);
void oled_rrect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t r, uint8_t color);
void oled_chelper( uint8_t x0, uint8_t y0, uint8_t r, uint8_t cornername, uint8_t color);
void oled_line(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t color);
void oled_vline(uint8_t x, uint8_t y, uint8_t h, uint8_t color);
void oled_hline(uint8_t x, uint8_t y, uint8_t w, uint8_t color);

void oled_bitmap(uint8_t x, uint8_t y, const unsigned char *bitmap, uint8_t w, uint8_t h, uint8_t color);
void oled_char(uint8_t x, uint8_t y, unsigned char c, uint8_t color, uint8_t bg, uint8_t size);
void oled_str(uint8_t x, uint8_t y, char *str, uint8_t color, uint8_t bg, uint8_t size);
void oled_int(uint8_t x, uint8_t y, int val, uint8_t color, uint8_t bg, uint8_t size);

#endif /*__oled_H */
