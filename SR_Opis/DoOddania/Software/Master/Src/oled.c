/*
 * oled.c
 *
 *  Created on: 10 wrz 2016
 *      Author: Marcin W�grzyn
 */
#include "oled.h"
#include "i2c.h"
#include "fonts.h"
#include <stdlib.h>
#include <string.h>

static uint8_t buffer[1024] = { };

static void oled_command(uint8_t cmd)
{
	HAL_I2C_Mem_Write(&hi2c1, OLED_ADDRESS, 0x00, 1, &cmd, 1, 100);
}

void oled_init(void)
{
	HAL_Delay(150);
	oled_command(OLED_DISPLAYOFF);                    // 0xAE
	oled_command(OLED_SETDISPLAYCLOCKDIV);            // 0xD5
	oled_command(0x80);                                  // the suggested ratio 0x80

	oled_command(OLED_SETMULTIPLEX);                  // 0xA8
	oled_command(OLED_LCDHEIGHT - 1);

	oled_command(OLED_SETDISPLAYOFFSET);              // 0xD3
	oled_command(0x0);                                   // no offset
	oled_command(OLED_SETSTARTLINE | 0x0);            // line #0
	oled_command(OLED_CHARGEPUMP);                    // 0x8D
	oled_command(0x14);
	oled_command(OLED_MEMORYMODE);                    // 0x20
	oled_command(0x00);                                  // 0x0 act like ks0108
	oled_command(OLED_SEGREMAP | 0x1);
	oled_command(OLED_COMSCANDEC);

	oled_command(OLED_SETCOMPINS);                    // 0xDA
	oled_command(0x12);
	oled_command(OLED_SETCONTRAST);                   // 0x81
	oled_command(0xCF);

	oled_command(OLED_SETPRECHARGE);                  // 0xd9

	oled_command(0xF1);
	oled_command(OLED_SETVCOMDETECT);                 // 0xDB
	oled_command(0x40);
	oled_command(OLED_DISPLAYALLON_RESUME);           // 0xA4
	oled_command(OLED_NORMALDISPLAY);                 // 0xA6

	oled_command(OLED_DEACTIVATE_SCROLL);

	oled_command(OLED_DISPLAYON);//--turn on oled panel
}


void oled_display(void)
{
	uint8_t m;

	for (m = 0; m < 8; m++)
	{
		oled_command(0xB0 + m);
		oled_command(0x00);
		oled_command(0x10);

		HAL_I2C_Mem_Write(&hi2c1, OLED_ADDRESS, 0x40, 1, &buffer[OLED_LCDWIDTH*m], OLED_LCDWIDTH, 10);
	}

}

void oled_fill(uint8_t color)
{
	  memset(buffer, color, 1024);
}

void oled_clear(void)
{
	  memset(buffer, 0, 1024);
}

void oled_pix(uint8_t x, uint8_t y, uint8_t color)
{
	if ((x < 0) || (x >= OLED_LCDWIDTH) || (y < 0) || (y >= OLED_LCDHEIGHT))
		return;

    switch (color)
    {
    	case WHITE:   buffer[x+ (y/8)*OLED_LCDWIDTH] |=  (1 << (y&7)); break;
    	case BLACK:   buffer[x+ (y/8)*OLED_LCDWIDTH] &= ~(1 << (y&7)); break;
    	case INVERSE: buffer[x+ (y/8)*OLED_LCDWIDTH] ^=  (1 << (y&7)); break;
    }
}


void oled_invert(void)
{
	uint16_t i;

	for (i = 0; i < 1024; i++)
		buffer[i] = ~buffer[i];
}

void oled_line(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t color)
{
	int16_t dx, dy, sx, sy, err, e2, i, tmp;

	/* Check for overflow */
	if (x0 >= OLED_LCDWIDTH) {
		x0 = OLED_LCDWIDTH - 1;
	}
	if (x1 >= OLED_LCDWIDTH) {
		x1 = OLED_LCDWIDTH - 1;
	}
	if (y0 >= OLED_LCDHEIGHT) {
		y0 = OLED_LCDHEIGHT - 1;
	}
	if (y1 >= OLED_LCDHEIGHT) {
		y1 = OLED_LCDHEIGHT - 1;
	}

	dx = (x0 < x1) ? (x1 - x0) : (x0 - x1);
	dy = (y0 < y1) ? (y1 - y0) : (y0 - y1);
	sx = (x0 < x1) ? 1 : -1;
	sy = (y0 < y1) ? 1 : -1;
	err = ((dx > dy) ? dx : -dy) / 2;

	if (dx == 0) {
		if (y1 < y0) {
			tmp = y1;
			y1 = y0;
			y0 = tmp;
		}

		if (x1 < x0) {
			tmp = x1;
			x1 = x0;
			x0 = tmp;
		}

		/* Vertical line */
		for (i = y0; i <= y1; i++) {
			oled_pix(x0, i, color);
		}

		/* Return from function */
		return;
	}

	if (dy == 0) {
		if (y1 < y0) {
			tmp = y1;
			y1 = y0;
			y0 = tmp;
		}

		if (x1 < x0) {
			tmp = x1;
			x1 = x0;
			x0 = tmp;
		}

		/* Horizontal line */
		for (i = x0; i <= x1; i++) {
			oled_pix(i, y0, color);
		}

		/* Return from function */
		return;
	}

	while (1) {
		oled_pix(x0, y0, color);
		if (x0 == x1 && y0 == y1) {
			break;
		}
		e2 = err;
		if (e2 > -dx) {
			err -= dy;
			x0 += sx;
		}
		if (e2 < dy) {
			err += dx;
			y0 += sy;
		}
	}
}

void oled_vline(uint8_t x, uint8_t y, uint8_t h, uint8_t color)
{
	oled_line(x, y, x, y+h-1, color);
}

void oled_hline(uint8_t x, uint8_t y, uint8_t w, uint8_t color)
{
	oled_line(x, y, x+w-1, y, color);
}

void oled_rect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color)
{
	/* Check input parameters */
	if (
		x >= OLED_LCDWIDTH ||
		y >= OLED_LCDHEIGHT
	) {
		/* Return error */
		return;
	}

	/* Check width and height */
	if ((x + w) >= OLED_LCDWIDTH) {
		w = OLED_LCDWIDTH - x;
	}
	if ((y + h) >= OLED_LCDHEIGHT) {
		h = OLED_LCDHEIGHT - y;
	}

	/* Draw 4 lines */
	oled_line(x, y, x + w, y, color);         /* Top line */
	oled_line(x, y + h, x + w, y + h, color); /* Bottom line */
	oled_line(x, y, x, y + h, color);         /* Left line */
	oled_line(x + w, y, x + w, y + h, color); /* Right line */
}

void oled_rrect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t r, uint8_t color)
{
	oled_hline(x+r  , y    , w-2*r, color); // Top
	oled_hline(x+r  , y+h-1, w-2*r, color); // Bottom
	oled_vline(x    , y+r  , h-2*r, color); // Left
	oled_vline(x+w-1, y+r  , h-2*r, color); // Right

	oled_chelper(x+r    , y+r    , r, 1, color);
	oled_chelper(x+w-r-1, y+r    , r, 2, color);
	oled_chelper(x+w-r-1, y+h-r-1, r, 4, color);
	oled_chelper(x+r , y+h-r-1, r, 8, color);
}

void oled_chelper( uint8_t x0, uint8_t y0, uint8_t r, uint8_t cornername, uint8_t color)
{
	int16_t f     = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x     = 0;
	int16_t y     = r;

	while (x<y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f     += ddF_y;
		}
		x++;
		ddF_x += 2;
		f     += ddF_x;
		if (cornername & 0x4)
		{
			oled_pix(x0 + x, y0 + y, color);
			oled_pix(x0 + y, y0 + x, color);
		}
		if (cornername & 0x2)
		{
			oled_pix(x0 + x, y0 - y, color);
			oled_pix(x0 + y, y0 - x, color);
		}
		if (cornername & 0x8)
		{
			oled_pix(x0 - y, y0 + x, color);
			oled_pix(x0 - x, y0 + y, color);
		}
		if (cornername & 0x1)
		{
			oled_pix(x0 - y, y0 - x, color);
			oled_pix(x0 - x, y0 - y, color);
		}
	}
}

void oled_frect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color)
{
	uint8_t i;

	/* Check input parameters */
	if (
		x >= OLED_LCDWIDTH ||
		y >= OLED_LCDHEIGHT
	) {
		/* Return error */
		return;
	}

	/* Check width and height */
	if ((x + w) >= OLED_LCDWIDTH) {
		w = OLED_LCDWIDTH - x;
	}
	if ((y + h) >= OLED_LCDHEIGHT) {
		h = OLED_LCDHEIGHT - y;
	}

	/* Draw lines */
	for (i = 0; i <= h; i++) {
		/* Draw lines */
		oled_line(x, y + i, x + w, y + i, color);
	}
}

void oled_triangle(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t x3, uint8_t y3, uint8_t color)
{
	/* Draw lines */
	oled_line(x1, y1, x2, y2, color);
	oled_line(x2, y2, x3, y3, color);
	oled_line(x3, y3, x1, y1, color);
}


void oled_ftriangle(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t x3, uint8_t y3, uint8_t color)
{
	int16_t deltax = 0, deltay = 0, x = 0, y = 0, xinc1 = 0, xinc2 = 0,
	yinc1 = 0, yinc2 = 0, den = 0, num = 0, numadd = 0, numpixels = 0,
	curpixel = 0;

	deltax = abs(x2 - x1);
	deltay = abs(y2 - y1);
	x = x1;
	y = y1;

	if (x2 >= x1) {
		xinc1 = 1;
		xinc2 = 1;
	} else {
		xinc1 = -1;
		xinc2 = -1;
	}

	if (y2 >= y1) {
		yinc1 = 1;
		yinc2 = 1;
	} else {
		yinc1 = -1;
		yinc2 = -1;
	}

	if (deltax >= deltay){
		xinc1 = 0;
		yinc2 = 0;
		den = deltax;
		num = deltax / 2;
		numadd = deltay;
		numpixels = deltax;
	} else {
		xinc2 = 0;
		yinc1 = 0;
		den = deltay;
		num = deltay / 2;
		numadd = deltax;
		numpixels = deltay;
	}

	for (curpixel = 0; curpixel <= numpixels; curpixel++) {
		oled_line(x, y, x3, y3, color);

		num += numadd;
		if (num >= den) {
			num -= den;
			x += xinc1;
			y += yinc1;
		}
		x += xinc2;
		y += yinc2;
	}
}

void oled_circle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color)
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

    oled_pix(x0, y0 + r, color);
    oled_pix(x0, y0 - r, color);
    oled_pix(x0 + r, y0, color);
    oled_pix(x0 - r, y0, color);

    while (x < y) {
        if (f >= 0) {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;

        oled_pix(x0 + x, y0 + y, color);
        oled_pix(x0 - x, y0 + y, color);
        oled_pix(x0 + x, y0 - y, color);
        oled_pix(x0 - x, y0 - y, color);

        oled_pix(x0 + y, y0 + x, color);
        oled_pix(x0 - y, y0 + x, color);
        oled_pix(x0 + y, y0 - x, color);
        oled_pix(x0 - y, y0 - x, color);
    }
}

void oled_fcircle(uint8_t x0, uint8_t y0, uint8_t r, uint8_t color)
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

    oled_pix(x0, y0 + r, color);
    oled_pix(x0, y0 - r, color);
    oled_pix(x0 + r, y0, color);
    oled_pix(x0 - r, y0, color);
    oled_line(x0 - r, y0, x0 + r, y0, color);

    while (x < y) {
        if (f >= 0) {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;

        oled_line(x0 - x, y0 + y, x0 + x, y0 + y, color);
        oled_line(x0 + x, y0 - y, x0 - x, y0 - y, color);

        oled_line(x0 + y, y0 + x, x0 - y, y0 + x, color);
        oled_line(x0 + y, y0 - x, x0 - y, y0 - x, color);
    }
}

void oled_bitmap(uint8_t x, uint8_t y, const unsigned char *bitmap, uint8_t w, uint8_t h, uint8_t color)
{
	int16_t byteWidth = (w + 7) / 8;
	uint8_t byte = 0, i, j;

	if(x == 255) x = (OLED_LCDWIDTH - w) / 2;
	if(y == 255) y = (OLED_LCDHEIGHT - h) / 2;

	for(j=0; j<h; j++)
	{
		for(i=0; i<w; i++ )
		{
			if(i & 7) byte <<= 1;
			else      byte   = bitmap[j * byteWidth + i / 8];
			if(byte & 0x80) oled_pix(x+i, y+j, color);
		}
	}
}


void oled_char(uint8_t x, uint8_t y, unsigned char c, uint8_t color, uint8_t bg, uint8_t size)
{
    if((x >= OLED_LCDWIDTH) || (y >= OLED_LCDHEIGHT) || ((x + 6 * size - 1) < 0) || ((y + 8 * size - 1) < 0))
      return;

    uint8_t i, j;

    for(i=0; i<6; i++ )
    {
      uint8_t line;
      if(i < 5) line = font[(c*5)+i];
      else      line = 0x0;
      for( j=0; j<8; j++, line >>= 1)
      {
        if(line & 0x1)
        {
          if(size == 1) oled_pix(x+i, y+j, color);
          else          oled_frect(x+(i*size), y+(j*size), size, size, color);
        } else if(bg != color)
        {
          if(size == 1) oled_pix(x+i, y+j, bg);
          else          oled_frect(x+i*size, y+j*size, size, size, bg);
        }
      }
    }
}

void oled_str(uint8_t x, uint8_t y, char *str, uint8_t color, uint8_t bg, uint8_t size)
{
	register char znak;
	uint8_t cnt = 0;
	if(x == 255) x = (OLED_LCDWIDTH - strlen(str)*6*size )/2;
	if(y == 255) y = (OLED_LCDHEIGHT - 5*size )/2;

	while ( (znak=*(str++)) )
	{
		oled_char(x+cnt*6*size, y, znak, color, bg, size);
		cnt++;
	}
}

void oled_int(uint8_t x, uint8_t y, int val, uint8_t color, uint8_t bg, uint8_t size)
{
	char bufor[17];
	oled_str(x, y, itoa(val, bufor, 10), color, bg, size);
}

void oled_on(void)
{
	oled_command(0x8D);
	oled_command(0x14);
	oled_command(0xAF);
}

void oled_off(void)
{
	oled_command(0x8D);
	oled_command(0x10);
	oled_command(0xAE);
}


