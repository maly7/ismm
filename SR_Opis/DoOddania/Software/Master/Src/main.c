/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "oled.h"
#include "rfm.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define RTC_ADDRESS 0xDE
#define EEPROM_ADDRESS 0xAE

#define KEY_UP 		0
#define KEY_DOWN 	1

#define X0	15
#define Y0 	15
#define X1 	90
#define Y1	60

uint8_t key[2];
uint8_t licznik = 0;

uint8_t hour = 0, min = 0, sec = 0;

int temp = 0, hum = 0, gas = 0, move = 0, id = 0, gid = 0, id_mode = 0;

volatile uint8_t keys_flag = 0, rtc_flag = 1, data_wait = 0, data_get = 0;

unsigned char tx_addr[5] = {0, 0, 0, 0, 2};
const unsigned char rx_addr[5] = {0, 0, 0, 0, 1};

void rx_usb_ready();
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
int _write(int file, char* ptr, int len)
{
	CDC_Transmit_FS(ptr, len);
	return len;
}

void display_data(void);
void key_pressed(uint8_t key);
void get_time(void);
void set_time(uint8_t hh, uint8_t mm, uint8_t ss);
void display_time(uint8_t x, uint8_t y, uint8_t size);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_SPI1_Init();
  MX_TIM17_Init();
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */
	oled_init();
	oled_clear();
	oled_str(103, 1, "ISMM", 1, 0, 1);
	get_time();
	display_time(0,1,1);
	oled_line(0, 10, 128, 10, 1);
	oled_display();

	HAL_TIM_Base_Start_IT(&htim17);

	rfm73_init();
	rfm73_mode_receive();
	rfm73_channel(107);

	rfm73_transmit_address( tx_addr );
	rfm73_receive_address_p0(rx_addr);
	unsigned char buf[32], tx_buf[32], pipe = 0, len = 0;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
	{
		if(data_wait)
		{
			uint8_t status = rfm73_register_read(RFM73_REG_STATUS);
			if(status & (1 << 6)) //RX data
			{
				rfm73_receive(&pipe, buf, &len);
				sscanf((char*)buf, "%d:%d;%d;%d;%d;END\r\n", &id, &temp,&hum,&gas,&move);
				if(id == gid)
				{
					display_data();
					int crc = id + (int)temp + (int)((temp*100)%100) + hum + gas + move;
					crc = crc%36;
					printf("MSTR;%d;%d;%d;%d;%d;%d;%d:END\n", id, (int)temp, (int)((temp*100)%100), hum, gas, move, crc);
					data_wait = 0;
				}
			}
			rfm73_register_write(RFM73_REG_STATUS, status); //clear ints
			if(data_wait == 1)
			{
				oled_str(255, 20, "                   ", 1, 0, 1);
				oled_str(255, 30, "      TIMEOUT      ", 1, 0, 1);
				oled_str(255, 40, "                   ", 1, 0, 1);
				oled_str(255, 50, "                   ", 1, 0, 1);

				int crc = id + (int)temp + (int)((temp*100)%100) + hum + gas + move;
				crc = (crc%36)+1;
				printf("MSTR;%d;0;0;0;0;0;%d:END\n", gid, gid-1);
				data_wait = 0;
			}
		}
		else if(data_get)
		{
			gid = data_get;
			data_get = 0;
			tx_addr[4] = gid;
			rfm73_transmit_address( tx_addr );
			rfm73_mode_transmit();
			HAL_Delay(50);
			len = sprintf((char*)tx_buf,"D;%d;G\r\n", gid);
			rfm73_transmit_message_once(tx_buf, len);
			HAL_Delay(20);
			rfm73_mode_receive();
			HAL_Delay(30);
			data_wait = 10; // czeka sekunde
		}

		if(rtc_flag)
		{
			rtc_flag = 0;
			get_time();
			display_time(0,1,1);
		}
		if(keys_flag)
		{
			keys_flag = 0;
			if(((key[KEY_UP] = (key[KEY_UP]<<1) | HAL_GPIO_ReadPin(KEY1_GPIO_Port, KEY1_Pin)) & 0x0F)  == 0x08) key_pressed(KEY_UP);
			if(((key[KEY_DOWN] = (key[KEY_DOWN]<<1) | HAL_GPIO_ReadPin(KEY2_GPIO_Port, KEY2_Pin)) & 0x0F)  == 0x08) key_pressed(KEY_DOWN);
			oled_display();
		}
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

	}
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

void display_data(void)
{
	char buf[32];
	sprintf(buf, "C%d", gid);
	oled_str(70,1, buf, 1, 0, 1);
	sprintf(buf, "     Temperatura: %d C     ", temp);
	oled_str(255,20, buf, 1, 0, 1);
	sprintf(buf, "     Humidity: %d %%     ", hum);
	oled_str(255,30, buf, 1, 0, 1);
	sprintf(buf, "     Gas: %d ppm     ", gas);
	oled_str(255,40, buf, 1, 0, 1);
	sprintf(buf, "     Move: %c     ", move?'Y':'N');
	oled_str(255,50, buf, 1, 0, 1);
}

void display_time(uint8_t x, uint8_t y, uint8_t size)
{
	char buff[10];
	sprintf(buff, "%02d:%02d:%02d", hour, min, sec);
	oled_str(x, y, buff, 1, 0, size);
}

void get_time(void)
{
	uint8_t read[3];
	HAL_I2C_Mem_Read(&hi2c1, RTC_ADDRESS, 0x00, 1, read, 3, 50);
	sec = ((read[0]&0x70)>>4)*10+(read[0]&0xF);
	min = ((read[1]&0x70)>>4)*10+(read[1]&0xF);
	hour = ((read[2]&0x30)>>4)*10+(read[2]&0xF);

}

void set_time(uint8_t hh, uint8_t mm, uint8_t ss)
{
	uint8_t write[4];
	write[0] = 0x80 | (ss/10)<<4 | ss%10;
	write[1] = (mm/10)<<4 | mm%10;
	write[2] = (hh/10)<<4 | hh%10;
	write[3] = 0x28;

	HAL_I2C_Mem_Write(&hi2c1, RTC_ADDRESS, 0x00, 1, write, 4, 50);
}

void key_pressed(uint8_t key)
{
	if(key == KEY_UP)
	{
		if(id_mode < 5) id_mode++;
		else id_mode = 0;
	}
	else if(key == KEY_DOWN)
	{
		data_get = id_mode;
	}

	char buf[32];
	sprintf(buf, "C%d", id_mode);
	oled_str(70,1, buf, 1, 0, 1);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	static uint8_t _100ms = 0;
	if(htim == &htim17)
	{
		keys_flag = 1;
		if(++_100ms%10 == 0)
		{
			if(data_wait) data_wait--;
			HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
		}
		if(_100ms%50 == 0) // co 500 ms
		{
			rtc_flag = 1;
			_100ms = 0;
		}
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
