#include "delay.h"

TIM_TypeDef *timer;

void Delay_Init(TIM_TypeDef *tim)
{
	timer = tim;
	TIM_HandleTypeDef htim;

    if (timer == TIM1) __HAL_RCC_TIM1_CLK_ENABLE();
    else if (timer == TIM3) __HAL_RCC_TIM3_CLK_ENABLE();
    else if (timer == TIM14) __HAL_RCC_TIM14_CLK_ENABLE();
    else if (timer == TIM16) __HAL_RCC_TIM16_CLK_ENABLE();
    else if (timer == TIM17) __HAL_RCC_TIM17_CLK_ENABLE();

    htim.Instance = timer;
    htim.Init.Prescaler = SystemCoreClock / 1000000 - 1;
    htim.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim.Init.Period = 10000 - 1;
    htim.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    htim.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim.Init.RepetitionCounter = 0;


    HAL_TIM_Base_Init(&htim);

    htim.State= HAL_TIM_STATE_BUSY;
    __HAL_TIM_ENABLE(&htim);
    htim.State= HAL_TIM_STATE_READY;
}

void Delay(unsigned int time)
{
    timer->CNT = 0;
    time -= 2;
    while (timer->CNT <= time) {}
}
