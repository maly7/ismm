/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "spi.h"
#include "tim.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "rfm.h"
#include "dht.h"
#include "onewire.h"
#include "ds18b20.h"
#include "delay.h"
#include <stdio.h>
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
DHT dht;
OWire OW;
uint8_t DS_ROM[8];

const int id = 2;

volatile uint8_t mq_start = 30, send_flag = 1, pomiar_flag = 1;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void inicjalizacja_rmf73(void);
void inicjalizacja_ds18b20(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC_Init();
  MX_SPI1_Init();
  MX_TIM17_Init();

  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start_IT(&htim17);

  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, 1);
  HAL_GPIO_WritePin(MQ2_GPIO_Port, MQ2_Pin, 1);

  uint16_t adc;
  HAL_ADC_Start_DMA(&hadc, (uint32_t*)&adc, 1);

  unsigned char buf[32], pipe = 0, tx_buf[32], len = 0;
  uint8_t hum = 0, temp = 0, move = 0;
  int gas = 0;
  int gid = 0;
  //float temp2 = 0;

  inicjalizacja_rmf73();
  DHT11_Init(&dht, DHT11_GPIO_Port, DHT11_Pin);
  //inicjalizacja_ds18b20();

  unsigned char cmd[2];
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	uint8_t status = rfm73_register_read(RFM73_REG_STATUS);
	if(status & (1 << 6)) //RX data
	{
		rfm73_receive(&pipe, buf, &len);
		sscanf((char*)buf, "%c;%d;%c\r\n", &cmd[0], &gid,&cmd[1]);
		if(cmd[0] == 'D' && cmd[1] == 'G' && gid == id)
			send_flag = 1;
	}
	rfm73_register_write(RFM73_REG_STATUS, status); //clear ints

	if(pomiar_flag)
	{
		pomiar_flag = 0;
		if(DHT11_RawRead(&dht, buf) == DHT11_OK)
		{
		  hum = DHT11_Humidity(buf);
		  temp = DHT11_Temperature(buf);
		}
		/*if(ds18b20_all_done(&OW))
		{
		  ds18b20_read(&OW, DS_ROM, &temp2);
		  ds18b20_start_all(&OW);
		}*/
	}
	if(HAL_GPIO_ReadPin(PIR_GPIO_Port, PIR_Pin)) move = 1;

	if(send_flag)
	{
		if(mq_start) gas = 0;
		else gas = adc;

		len = sprintf((char*)tx_buf,"%d:%d;%d;%d;%d;END\r\n", id, temp, hum, gas, move);
		move = 0;

		rfm73_mode_transmit();
		HAL_Delay(100);
		HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, 1);
		rfm73_transmit_message_once(tx_buf, len);
		HAL_Delay(50);
		HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, 0);
		rfm73_mode_receive();

		send_flag = 0;
	}
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void inicjalizacja_rmf73(void)
{
	rfm73_init();
	/*if(rfm73_is_present())
	{
		uint8_t i;
		for(i=0;i<4;i++)
		{
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, (i%2));
			HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, !(i%2));
			HAL_Delay(100);
		}
	}*/
	rfm73_mode_receive();		// reveive mode
	rfm73_channel(107);
	const unsigned char rx_addr[5] = {0, 0, 0, 0, id};
	const unsigned char tx_addr[5] = {0, 0, 0, 0, 1};
	rfm73_transmit_address( tx_addr );
	rfm73_receive_address_p0( rx_addr );
}

void inicjalizacja_ds18b20(void)
{
	Delay_Init(TIM14);
	OWInit(&OW, ONE_WIRE_GPIO_Port, ONE_WIRE_Pin);
	if(OWFirst(&OW, DS_ROM))
	{
		if (is_ds18b20(DS_ROM))
		{
			ds18b20_setresolution(&OW, DS_ROM, DS18B20_Resolution_12bits);
			ds18b20_start_all(&OW);
		}
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	static uint8_t _100ms = 0;
	if(htim == &htim17)
	{
		if(++_100ms%5 == 0) // co 500ms
		{
			pomiar_flag = 1;
		}
		if(_100ms == 10) // co 1000ms
		{
			_100ms = 0;
			if(mq_start) mq_start--;
		}
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
