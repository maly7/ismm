#include "ds18b20.h"

uint8_t ds18n20_start(OWire* owire, uint8_t *rom)
{
	/* Check if device is DS18B20 */
	if (!is_ds18b20(rom)) {
		return 0;
	}

	/* Reset line */
	OWReset(owire);
	/* Select ROM number */
	OWSelect(owire, rom);
	/* Start temperature conversion */
	OWWrite(owire, DS18B20_CMD_CONVERTTEMP);

	return 1;
}

void ds18b20_start_all(OWire* owire)
{
	/* Reset pulse */
	OWReset(owire);
	/* Skip rom */
	OWWrite(owire, ONEWIRE_CMD_SKIPROM);
	OWWrite(owire, DS18B20_CMD_CONVERTTEMP);
}

uint8_t ds18b20_read(OWire* owire, uint8_t *rom, float *destination)
{
	uint16_t temperature;
	uint8_t resolution;
	int8_t digit, minus = 0;
	float decimal;
	uint8_t data[9];
	uint8_t crc;

	/* Check if device is DS18B20 */
	if (!is_ds18b20(rom))
		return 0;

	/* Check if line is released, if it is, then conversion is complete */
	if (!OWRead_bit(owire))
		/* Conversion is not finished yet */
		return 0;

	/* Reset line */
	OWReset(owire);
	/* Select ROM number */
	OWSelect(owire, rom);
	/* Read scratchpad command by onewire protocol */
	OWWrite(owire, ONEWIRE_CMD_RSCRATCHPAD);

	/* Get data */
	OWRead_bytes(owire, data, 9);

	/* Calculate CRC */
	crc = OWCrc8(data, 8);

	/* Check if CRC is ok */
	if (crc != data[8])
		/* CRC invalid */
		return 0;

	/* First two bytes of scratchpad are temperature values */
	temperature = data[0] | (data[1] << 8);

	/* Reset line */
	OWReset(owire);

	/* Check if temperature is negative */
	if (temperature & 0x8000) {
		/* Two's complement, temperature is negative */
		temperature = ~temperature + 1;
		minus = 1;
	}

	/* Get sensor resolution */
	resolution = ((data[4] & 0x60) >> 5) + 9;

	/* Store temperature integer digits and decimal digits */
	digit = temperature >> 4;
	digit |= ((temperature >> 8) & 0x7) << 4;

	/* Store decimal digits */
	switch (resolution) {
		case 9: {
			decimal = (temperature >> 3) & 0x01;
			decimal *= (float)DS18B20_DECIMAL_STEPS_9BIT;
		} break;
		case 10: {
			decimal = (temperature >> 2) & 0x03;
			decimal *= (float)DS18B20_DECIMAL_STEPS_10BIT;
		} break;
		case 11: {
			decimal = (temperature >> 1) & 0x07;
			decimal *= (float)DS18B20_DECIMAL_STEPS_11BIT;
		} break;
		case 12: {
			decimal = temperature & 0x0F;
			decimal *= (float)DS18B20_DECIMAL_STEPS_12BIT;
		} break;
		default: {
			decimal = 0xFF;
			digit = 0;
		}
	}

	/* Check for negative part */
	decimal = digit + decimal;
	if (minus) {
		decimal = 0 - decimal;
	}

	/* Set to pointer */
	*destination = decimal;

	/* Return 1, temperature valid */
	return 1;
}

uint8_t ds18b20_setresolution(OWire* owire, uint8_t *rom, uint8_t resolution)
{
	uint8_t th, tl, conf;
	if (!is_ds18b20(rom))
		return 0;

	/* Reset line */
	OWReset(owire);
	/* Select ROM number */
	OWSelect(owire, rom);
	/* Read scratchpad command by onewire protocol */
	OWWrite(owire, ONEWIRE_CMD_RSCRATCHPAD);

	/* Ignore first 2 bytes */
	OWRead(owire);
	OWRead(owire);

	th = OWRead(owire);
	tl = OWRead(owire);
	conf = OWRead(owire);

	if (resolution == DS18B20_Resolution_9bits) {
		conf &= ~(1 << DS18B20_RESOLUTION_R1);
		conf &= ~(1 << DS18B20_RESOLUTION_R0);
	} else if (resolution == DS18B20_Resolution_10bits) {
		conf &= ~(1 << DS18B20_RESOLUTION_R1);
		conf |= 1 << DS18B20_RESOLUTION_R0;
	} else if (resolution == DS18B20_Resolution_11bits) {
		conf |= 1 << DS18B20_RESOLUTION_R1;
		conf &= ~(1 << DS18B20_RESOLUTION_R0);
	} else if (resolution == DS18B20_Resolution_12bits) {
		conf |= 1 << DS18B20_RESOLUTION_R1;
		conf |= 1 << DS18B20_RESOLUTION_R0;
	}

	/* Reset line */
	OWReset(owire);
	/* Select ROM number */
	OWSelect(owire, rom);
	/* Write scratchpad command by onewire protocol, only th, tl and conf register can be written */
	OWWrite(owire, ONEWIRE_CMD_WSCRATCHPAD);

	/* Write bytes */
	OWWrite(owire, th);
	OWWrite(owire, tl);
	OWWrite(owire, conf);

	/* Reset line */
	OWReset(owire);
	/* Select ROM number */
	OWSelect(owire, rom);
	/* Copy scratchpad to EEPROM of DS18B20 */
	OWWrite(owire, ONEWIRE_CMD_CPYSCRATCHPAD);

	return 1;
}

uint8_t ds18b20_all_done(OWire* owire)
{
	/* If read bit is low, then device is not finished yet with calculation temperature */
	return OWRead_bit(owire);
}


uint8_t is_ds18b20(uint8_t *rom)
{
	/* Checks if first byte is equal to DS18B20's family code */
	if (*rom == DS18B20_FAMILY_CODE)
		return 1;
	return 0;
}
