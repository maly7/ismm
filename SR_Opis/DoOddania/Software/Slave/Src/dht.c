/*
 * dht.c
 *
 *  Created on: 07.05.2017
 *      Author: maly
 */

#include "dht.h"

void DHT11_Init(DHT* dht, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
  if (GPIOx == GPIOA) __HAL_RCC_GPIOA_CLK_ENABLE();
  else if (GPIOx == GPIOB) __HAL_RCC_GPIOB_CLK_ENABLE();
  else if (GPIOx == GPIOC) __HAL_RCC_GPIOC_CLK_ENABLE();
  else if (GPIOx == GPIOD) __HAL_RCC_GPIOD_CLK_ENABLE();

  GPIO_InitTypeDef  GPIO_InitStruct;
  GPIO_InitStruct.Pin = GPIO_Pin;
  GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);

  dht->m_BitMask = GPIO_Pin;
  dht->m_Port = GPIOx;

  uint8_t pin = 0;

  while(GPIO_Pin != (1<<pin)) pin++;

  dht->m_Register = &GPIOx->MODER;
  dht->m_IoMask =  (0x1<<2*pin);
}

void DHT11_Input(DHT* dht)
{
	*dht->m_Register &= ~dht->m_IoMask;
}

void DHT11_Output(DHT* dht)
{
	*dht->m_Register |= dht->m_IoMask;
}

uint8_t DHT11_ReadPin(DHT* dht)
{
  return (uint8_t)((dht->m_Port->IDR & dht->m_BitMask) > 0 ? 1 : 0);
}

void DHT11_WriteHigh(DHT* dht)
{
  dht->m_Port->BSRR = dht->m_BitMask;
}

void DHT11_WriteLow(DHT* dht)
{
  dht->m_Port->BRR = dht->m_BitMask;
}


uint16_t read_cycle(DHT *dht, uint16_t cur_tics, uint8_t neg_tic)
{
	uint16_t cnt_tics;
 	if (cur_tics < MAX_TICS) cnt_tics = 0;
	if(neg_tic)
	{
		while (!DHT11_ReadPin(dht)&&(cnt_tics<MAX_TICS))
			cnt_tics++;
	}
	else
	{
		while (DHT11_ReadPin(dht)&&(cnt_tics<MAX_TICS))
			cnt_tics++;
	}
 	return cnt_tics;
}

uint8_t DHT11_RawRead(DHT *dht, uint8_t *buf)
{
	uint16_t dt[42];
	uint16_t cnt = 0;
	uint8_t i, check_sum = 0;

	DHT11_Output(dht);
	//reset DHT11
	HAL_Delay(250);
	DHT11_WriteLow(dht);
	HAL_Delay(20);
	DHT11_WriteHigh(dht);
	DHT11_Input(dht);

	for(i=0;i<83 && cnt<MAX_TICS;i++)
	{
		if (i & 1)
			cnt = read_cycle(dht, cnt, 1);
		else
		{
			cnt = read_cycle(dht, cnt, 0);
			dt[i/2]= cnt;
		}
	}

	DHT11_WriteHigh(dht);

	for (i = 0; i<5; i++) buf[i]=0;

	if (cnt>=MAX_TICS) return 	DHT11_NO_CONN;

	//convert data
 	for(i=2;i<42;i++)
 	{
		(*buf) <<= 1;
		if (dt[i]>50)
			(*buf)++;
		if (!((i-1)%8) && (i>2))
			buf++;
 	}

	//calculate checksum
	buf -= 5;
 	for(i=0;i<4;i++)
 	{
		check_sum += *buf;
		buf++;
	}

	//if (*buf != check_sum) return DHT11_CS_ERROR;

	return DHT11_OK;
}

uint8_t DHT11_Humidity(uint8_t *buf)
{
	return buf[0];
}

uint8_t DHT11_Temperature(uint8_t *buf)
{
	return buf[2];
}
